# FRAPC

Elementary FRAP simulation built in C using GLFW

## Build

```
mkdir build
cd build
cmake ..
make
```

## Integrate with your tools

## Authors and acknowledgment

FJN, 13.11.2021

## License

This is Open Source
